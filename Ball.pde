class Ball extends Block {
  int xVel =1;
  int yVel =1;
  Ball() {
    x = int(random(0, width - w));
    y = int(random(0, height - w));

    if (int (random(2)) ==0) {
      xVel =int( random(1, 5));
    } else {
      xVel = int( random(-5, -1));
    }
    yVel = -6;

    //w = int(random(5, 30));
    h = w;
  }

  void update(Paddle player, Brick[] bricks) {    
    super.update();
    if (!alive) {
      return;
    }
    x = x + xVel;
    y = y + yVel;

    if (x + w > width || x < 0) {
      xVel = -xVel;
    }

    if (y + w > height) {
      alive = false;
      //yVel = -yVel;
    }

    if (y < 0) {
      yVel = -yVel;
    }

    CheckCollisions(player, bricks);
  }

  void CheckCollisions(Paddle player, Brick[] bricks) {
    for (int i = 0; i < bricks.length; i++) {
      if (bricks[i].alive) {
        if (DoesCollide(bricks[i])) {
          bricks[i].Kill();
          yVel = -yVel;
        }
      }
    }

    if (DoesCollide(player)) {
      yVel = -yVel;
      int distance = CenterX() - player.CenterX();
      if (distance > 10 || distance < -10){
        xVel += int(distance / 8);
      }
      //print(distance);
    }
  }
}
