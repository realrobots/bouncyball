Ball[] balls =new Ball[32];
Brick[] bricks =new Brick[54];
Paddle player;

int ballCount = 1;

void setup() {

  size(1024, 768);
  
  player = new Paddle();
  
  for (int i = 0; i < balls.length; i++) {
    balls[i] = new Ball();
    if (i >= ballCount){
      balls[i].Kill();
    }
  }
  int w = width/10;
  for (int i = 0; i < bricks.length; i++) {
    bricks[i] = new Brick(player, balls);
    if (int(random(0, 6)) == 0){
      bricks[i] = new BrickShrink(player, balls);
    }
    if (int(random(0, 12)) == 0){
      bricks[i] = new BrickGrow(player, balls);
    }
    if (int(random(0, 5)) == 0){
      bricks[i] = new BrickBonusBall(player, balls);
    }
    bricks[i].w = w;
  }
  int c = 0;
  for (int y = 0; y < 6; y++) {
    for (int x = 0; x < 9; x++) {
      if (y % 2 == 0) {
        bricks[c].x = x * (width/9);
      } else {
        bricks[c].x = x * (width/9) + 20;
      }
      bricks[c].y = y * (bricks[c].h + 5);
      c++;
    }
  }
  
  
}
void draw() {
  background(1000000000);

  for (int i = 0; i < balls.length; i++) {
    balls[i].update(player, bricks);
  }
  for (int i = 0; i < bricks.length; i++) {
    bricks[i].update();
  }
  player.update();
  
  
}
