**Processing Brick Game**

This game was made in processing by students in RealRobots STEM classes

To run it download [Processing](https://processing.org/download) and use it to open this entire folder after downloading and extracting.

![alt text](screenshot.jpg "Bouncyball")