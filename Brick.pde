class Brick extends Block {
  Paddle player;
  Brick(Paddle player, Ball[] balls) {
    this.player = player;
  }

  void update() {
    if (!alive) {
      return;
    }
    fill(125);
    rect(x, y, w, h);
  }
}

class BrickShrink extends Brick {
  BrickShrink(Paddle player, Ball[] balls) {
    super(player, balls);
  }

  void update() {
    if (!alive) {
      return;
    }
    fill(255, 0, 0);
    rect(x, y, w, h);
  }

  void Kill() {
    super.Kill();
    player.w = int(player.w * 0.75);
  }
}

class BrickGrow extends Brick {
  BrickGrow(Paddle player, Ball[] balls) {
    super(player, balls);
  }

  void update() {
    if (!alive) {
      return;
    }
    fill(0, 255, 0);
    rect(x, y, w, h);
  }

  void Kill() {
    super.Kill();
    player.w = int(player.w * 1.2);
  }
}

class BrickBonusBall extends Brick {
  int bonusBalls = 1;
  BrickBonusBall(Paddle player, Ball[] balls) {
    super(player, balls);
    
    if (int (random(0, 10)) == 0){
      bonusBalls = 10;
    }
  }

  void update() {
    if (!alive) {
      return;
    }
    fill(0, 255, 255);
    rect(x, y, w, h);
  }

  void Kill() {
    super.Kill();
    for (int i = 0; i < balls.length; i++) {
      if (!balls[i].alive) {
        balls[i].alive = true;
        balls[i].x = CenterX();
        balls[i].y = CenterY();
        if (int (random(2)) ==0) {
          balls[i].xVel =int( random(1, 5));
        } else {
          balls[i].xVel = int( random(-5, -1));
        }
        balls[i].yVel = -4;
        
        bonusBalls-=1;
        if (bonusBalls == 0){
          return;
        }
      }
    }
  }
}
