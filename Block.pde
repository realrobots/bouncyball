class Block {
  int x = 0;
  int y = 0;
  int w = 20;
  int h = 20;
  boolean alive = true;

  Block() {
  }


  void update() {
    if (!alive) {
      return;
    }
    fill(255);
    rect(x, y, w, h);
  }

  // Checks if a point is inside a rectangle
  // Used to check if each corner of a ball collides
  boolean Contains(int pX, int pY) {
    if (pX > x && pX < x + w) {
      if (pY > y && pY < y + h) {
        return true;
      }
    }
    return false;
  }

  // Check if any corner of a block collides with this one
  boolean DoesCollide(Block block) {
    if (block.Contains(x, y) || 
      block.Contains(x + w, y) ||
      block.Contains(x + w, y + w) || 
      block.Contains(x, y + h)) {
      return true;
    }
    return false;
  }
  
  void Kill(){
    alive = false;
    println("KILLED");
  }
  
  int CenterX(){
     return int(x + w/2); 
  }
  int CenterY(){
     return int(y + h/2); 
  }
}
