class Paddle extends Block {
  Paddle() {
    x = width /2;
    y = height - 40;
    w = 100;
    h = 20;
  }

  void update() {
    super.update();
    
    if (w < 20){
      w = 20;
    }

    x = mouseX - w/2;
  }
  
}
